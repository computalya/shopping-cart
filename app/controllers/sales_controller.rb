class SalesController < ApplicationController
  def index
    @products = Product.order(:title).to_a
  end
end