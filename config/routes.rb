Rails.application.routes.draw do
  resources :products 

  resources :carts do
    resources :cartships
    # member do
    #   post :pay
    #   post :remove_item
    # end
  end

  get 'sales/index'

  root 'sales#index'
end